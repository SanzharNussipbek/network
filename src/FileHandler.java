import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FileHandler {

	private Config config;

	public FileHandler() {
		config = new Config();
	}

	public String readFromFile(File file) {
		String content = "";

		try {
			FileInputStream in = new FileInputStream(file);

			byte[] buffer = new byte[1024];

			int len = in.read(buffer);

			while (len > 0) {
				content += new String(buffer, 0, len);
				len = in.read(buffer);
			}

			in.close();
		} catch (IOException e) {
			return "Unable to read from file.";
		}

		return content;
	}

	public void writeToFile(File file, String text) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
			writer.write(text);
			writer.close();
		} catch (IOException e) {	}
	}

	public int validateFile(File file) {
		if (!file.exists()) return 1;

		if (!file.isFile()) return 2;

		if (!file.canRead()) return 3;

		return 0;
	}

	public int checkFileForDel(File file) {
		if (!file.exists()) return 1;

		if (file.isDirectory()) return 4;

		return 0;
	}

	public int checkDirForRD(File dir) {

		if (!dir.exists()) return 5;

		if (!dir.isDirectory()) return 6;

		if (dir.listFiles().length != 0) return 7;

		return 0;
	}

	public String getFullPath(String[] paths) {
		String path = paths[0];

		for (int j = 1; j < paths.length; j++) {

			path += "/" + paths[j];

		}

		return path;
	}

	public String prepareFilename(String filemame) {
		if (filemame.charAt(0) == '\\' || filemame.charAt(0) == '/') {

			filemame = filemame.substring(1);

		}

		return filemame.replaceAll(Pattern.quote("\\"), "/");
	}

	public int createFile(String filename) {
		File file = new File(filename);

		try {
			file.createNewFile();
		} catch (IOException e) {
			return 8;
		}

		return 0;
	}

	public static String getTime(Long time) {
		long HH = (time / (1000 * 60 * 60)) % 24;
		long MM = (time / (1000 * 60)) % 60;
		long SS = (time / 1000) % 60;
		long MS = time % 1000;

		return String.format("%02d:%02d:%02d.%d", HH, MM, SS, MS);
	}

	public String filesToTransfer(String[] args) {
		File[] files = new File[args.length - 1];

		for (int i = 1; i < args.length; i++) {
			files[i - 1] = new File(args[i].trim());
		}

		String res = "";
		String details = "";
		byte[] buffer;
		long size;

		for (File file : files) {
			if (validateFile(file) != 0) {
				res += config.MESSAGES[validateFile(file)] + "\n";
				continue;
			}

			try {
				FileInputStream in = new FileInputStream(file);
				buffer = new byte[1024];
				size = file.length();

				details += file.getName() + "\n";
				details += String.valueOf(size) + "\n";

				while (size > 0) {
					int len = in.read(buffer);
					details += new String(buffer, 0, len);
					size -= len;
				}

				res += details;
				details = "";
				in.close();
			} catch (IOException e) {
				continue;
			}
		}

		return res;
	}

	public String getFileToTransfer(String filename) {
		File file = new File(filename);
		if (validateFile(file) != 0) {
			return config.MESSAGES[validateFile(file)];
		}

		String details = "";

		try {
			FileInputStream in = new FileInputStream(file);
			byte[] buffer = new byte[1024];
			long size = file.length();

			details += file.getName() + "\n";

			while (size > 0) {
				int len = in.read(buffer);
				details += new String(buffer, 0, len);
				size -= len;
			}
			in.close();

			return details;
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	public void printBlock(String text) {
		System.out.println("==========================================================\n");
		System.out.println(text);
		System.out.println("\n==========================================================\n");
	}

	public String upload(String filename, DataOutputStream out) {
    File file = new File(filename);

		try {
			byte[] buffer = new byte[1024];

			FileInputStream in = new FileInputStream(file);

			out.writeInt(file.getName().length());

			out.write(file.getName().getBytes());

			long size = file.length();
			out.writeLong(size);

			int len = in.read(buffer, 0, buffer.length);

			while (len > 0) {
				out.write(buffer, 0, len);
				len = in.read(buffer, 0, buffer.length);
			}

			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		file.delete();

		return "";
	}

	public String download(DataInputStream in, boolean isServer) {
		byte[] buffer = new byte[1024];
		String action = isServer ? "Upload" : "Download";
		String filename = "";

    try {
			int nameLen = in.readInt();
			in.read(buffer, 0, nameLen);
			filename = new String(buffer, 0, nameLen);

			long size = in.readLong();

			File file = new File(filename);
			FileOutputStream outFile = new FileOutputStream(file);

			while(size > 0) {

        int len = in.read(buffer, 0, buffer.length);
				outFile.write(buffer, 0, len);
				size -= len;

      }

			outFile.close();
			unzipFiles();
		} catch (IOException e) {
			e.printStackTrace();
      return "Unable to " + action.toLowerCase() + " the files.\n";
		}

		return ("Files " + action.toLowerCase() + "ed");
	}

	public String zipFiles(String[] filenames) {
		byte[] bytes;
		String res = "";

		try {
				File zipFile = new File(config.zipFileName);
				FileOutputStream outFile = new FileOutputStream(zipFile);
				ZipOutputStream out = new ZipOutputStream(outFile);
				File file;

				for (String filename : filenames) {
					file = new File(filename);

					if (validateFile(file) != 0) {
						res += config.MESSAGES[validateFile(file)];
						continue;
					}

					out.putNextEntry(new ZipEntry(file.getName()));
					bytes = Files.readAllBytes(Paths.get(filename));
					out.write(bytes, 0, bytes.length);
					out.closeEntry();
				}

				out.close();

		} catch (IOException e) {
				res += config.MESSAGES[1];
		}

		return res;
	}

	public void unzipFiles() throws IOException{
		ZipInputStream in = new ZipInputStream(new FileInputStream(config.zipFileName));
		ZipEntry entry = in.getNextEntry();

		while (entry != null) {
				String filename =  entry.getName();
				extractFile(in, filename);
				in.closeEntry();
				entry = in.getNextEntry();
		}

		in.close();

		new File(config.zipFileName).delete();
	}

	private void extractFile(ZipInputStream in, String filename) throws IOException{
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(filename));
		byte[] bytes = new byte[1024];
		int len = in.read(bytes);
		while (len  > 0) {
			out.write(bytes, 0, len);
				len = in.read(bytes);
		}
		out.close();
	}

}
