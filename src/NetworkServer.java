import java.io.*;
import java.net.*;
import java.util.*;

public class NetworkServer {

  private ServerSocket TCPsocket;
  private DatagramSocket UDPsocket;
	private ArrayList<Client> clients = new ArrayList<Client>();
	private Config config;
  private MyCMD cmd;
  private FileHandler fileHandler;
  private Scanner scanner;
  private Client clientObj;

  public NetworkServer() {
		config = new Config();
    cmd = new MyCMD();
    fileHandler = new FileHandler();
    scanner = new Scanner(System.in);
    clientObj = new Client();

    try {
			UDPsocket = new DatagramSocket(config.UDP_PORT);
		} catch (SocketException e) {
			// System.out.println("Oops.. Error while creating UDP socket");
      // e.printStackTrace();
      System.exit(-1);
		}

		try {
			TCPsocket = new ServerSocket(config.TCP_PORT);
		} catch (IOException e) {
      // System.out.println("Oops.. Error while creating TCP socket");
      // e.printStackTrace();
      System.exit(-1);
    }
  }

  public void serve(){
    TCPHandler();
  }

  private void TCPHandler() {
    byte[] buffer = new byte[1024];

    try {
      while (true) {
        System.out.printf("Listening at port %d...\n", config.TCP_PORT);

        Socket clientSocket = TCPsocket.accept();

        System.out.printf("Established a connection to host %s: %d\n\n", clientSocket.getInetAddress(),
            clientSocket.getPort());

        new Thread(() -> {
          try {
            DataInputStream in = new DataInputStream(clientSocket.getInputStream());
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());

            while (true) {
              int len = in.readInt();
              in.read(buffer, 0, len);

              String str = new String(buffer, 0, len);

              String result = executeCommand(str, in, out);

              if (!result.isEmpty()) {
                sendMsgTCP(out, result);
              }
            }
          } catch (IOException e) {
            System.out.println("Connection dropped: " + clientSocket.getInetAddress());
          }

          try {
            clientSocket.close();
          } catch (IOException e) {
            // System.out.println("Socket already closed.");
          }
        }).start();
      }
    } catch(IOException e) {
      // System.out.println("Oops.. Error while serving TCP server");
      System.exit(-1);
    }
  }

  private void sendMsgTCP(DataOutputStream out, String msg) throws IOException {
    out.writeInt(msg.length());
    out.write(msg.getBytes(), 0, msg.length());
  }

  private String executeCommand(String command, DataInputStream in, DataOutputStream out) {
    String[] args = cmd.get_args(command);

    if (args[0].equals("upload")) {
      return fileHandler.download(in, true);
    }

    if (args[0].equals("download")) {
      try {
        String[] fileNames = Arrays.copyOfRange(args, 1, args.length);

        String zipResult = fileHandler.zipFiles(fileNames);

        if (!zipResult.isEmpty()) {
          return zipResult;
        }
        sendMsgTCP(out, "download");

        try {
          Thread.sleep(300);
        } catch (InterruptedException e) {}

        String response = fileHandler.upload(config.zipFileName, out);

        if (response.length() > 0) return response;

        return "";
      } catch (IOException e) {
        e.printStackTrace();
        return "Unable to download the file" + (args.length > 2 ? "s" : "" );
      }
    }

    return cmd.handleCommand(args);
  }




  public void UDPhandler() throws IOException {

		new Thread(() -> {
			broadcastMsg(config.BROADCAST_MESSAGE);
		}).start();

		receiveUDP();

	}

	public void broadcastMsg(String message) {
		try {
			byte[] msg = message.getBytes();

			InetAddress dest = InetAddress.getByName("255.255.255.255");

			DatagramPacket packet = new DatagramPacket(msg, msg.length, dest, config.UDP_PORT);

			System.out.println("Broadcasting UDP...");
			UDPsocket.send(packet);
		} catch (IOException e) {
			System.out.println("Oops.. Error while broadcasting");
			e.printStackTrace();
		}
	}

	public void sendMsgUDP(String msg, String ipAddress) throws IOException {
		InetAddress destination = InetAddress.getByName(ipAddress);

		DatagramPacket packet = new DatagramPacket(msg.getBytes(), msg.length(), destination, config.UDP_PORT);

		UDPsocket.send(packet);
	}

	public void receiveUDP() {
		try {

			DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);

			UDPsocket.receive(packet);

			byte[] data = packet.getData();

			String str = new String(data, 0, packet.getLength());

			if (str.equals(config.BROADCAST_MESSAGE)
					&& !packet.getAddress().equals(InetAddress.getLocalHost())) {

				System.out.println("Received UDP broadcass");

				String computerName = getComputerName();

				System.out.println("Trying to establish a connection with " + packet.getAddress());

				sendMsgUDP(computerName, packet.getAddress().toString());
			}

			else {
				String computerName = str;

				String ipAddress = packet.getAddress().toString().trim();

				Client client = new Client(ipAddress, computerName);

				synchronized (clients) {
					clients.add(client);
				}
			}
		} catch (IOException e) {

			System.out.println("Oops.. Error");
			e.printStackTrace();

		}
	}

	private String getComputerName() throws UnknownHostException {
    return InetAddress.getLocalHost().getHostName();
	}

}
