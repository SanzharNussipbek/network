import java.util.HashMap;
import java.util.Map;

public class Config {

	public int UDP_PORT = 9998;
	public int TCP_PORT = 9001;
	public String BROADCAST_MESSAGE = "BROADCAST";
	public String zipFileName = "network_files.zip";
	public String[] MESSAGES = {
		"ok",
		"ERROR: File does not exist",
		"ERROR: Not a file",
		"ERROR: Cannot read the file",
		"ERROR: To delete a directory, you should use RD command",
		"ERROR: Directory does not exist",
		"ERROR: To delete a file, you should use DEL command",
		"ERROR: Can delete only empty directories",
		"ERROR: File already exists",
	};

	public Map<String, String> accounts = new HashMap<String, String>() {
		{
			put("admin", "admin");
			put("member", "member");
			put("test", "test");
		}
	};

	public boolean validLogin(String username, String password) {
    return accounts.get(username) != null && accounts.get(username).equals(password);
	}

	/**
   * Function to validate the IP address
   * @param ip
   * @return
   */
  public boolean isValidIP(String ip) {
    // Max is XXX.XXX.XXX.XXX -> 15 characters
    // Min is X.X.X.X         -> 7 characters
    if (ip.length() > 15 || ip.length() < 7)
      return false;

    // Split the blocks by a dot
    String[] blocks = ip.split("\\.");

    // Check the number of blocks
    if (blocks.length != 4)
      return false;

    int num;

    // Convert string block to num and check it
    for (String block : blocks) {
      try {
        num = Integer.parseInt(block);
      } catch (NumberFormatException nfe) {
        return false;
      }
      if (num > 255)
        return false;
    }

    return true;
	}

	public Config() {}

}