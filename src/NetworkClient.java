import java.io.*;
import java.net.*;
import java.util.*;

public class NetworkClient {

  private Config config;
  private MyCMD cmd;
  private Socket socket;
  private Scanner scanner;
  private DataOutputStream out;
  private FileHandler fileHandler;
  private boolean running = true;
  private boolean serving = true;

  public NetworkClient(String serverIP) {
    config = new Config();
    cmd = new MyCMD();
    fileHandler = new FileHandler();

    try {
      socket = new Socket(serverIP, config.TCP_PORT);
      out = new DataOutputStream(socket.getOutputStream());
    } catch (IOException e) {
      System.out.println("Oops.. Error while creating TCP socket");
      // e.printStackTrace();
      System.exit(-1);
    }
  }

  public void serve() {
    Thread t = new Thread(() -> {
      receiveData();
    });
    t.start();

    System.out.print("Please input a command:\n>");
    scanner = new Scanner(System.in);
    String input = scanner.nextLine().trim();

    while (!input.equals("exit") && serving) {
      if (!input.isEmpty()) {
        String command = input.split(" ")[0];

        if (command.equals("upload")) {
          handleUpload(input);
        }

        else if (command.equals("download")) {
          handleDownload(input);
        }

        else if (command.equals("cls")) {
          cmd.executeCommand(input.split(" "));
        }

        else {
          sendMsg(input);
        }

        try {
          Thread.sleep(300);
        } catch (InterruptedException e) {}

      }

      System.out.print(">");
      input = scanner.nextLine().trim();
    }

    t.interrupt();

    destroy();
  }

  public void destroy() {
    running = false;
    scanner.close();

    try {
      socket.close();
    } catch (IOException e) {
      System.out.println("Oops.. Error while closing TCP socket");
      System.exit(-1);
    }
  }

  private void sendMsg(String msg) {
    try {
      out.writeInt(msg.length());
      out.write(msg.getBytes(), 0, msg.length());
    } catch (IOException e) {
      System.out.println("Unable to send message to the server!\n");
      e.printStackTrace();
    }
  }

  private void receiveData() {
    try {
      byte[] buffer = new byte[1024];
      DataInputStream in = new DataInputStream(socket.getInputStream());

      while (running && !Thread.interrupted()) {

        int len = in.readInt();
        int readSize;
        String msg = "";

        while (len > 0) {

          readSize = in.read(buffer, 0, 1024);
          msg += new String(buffer, 0, readSize);
          len -= readSize;

        }

        if (msg.trim().equals("download")) {
          download(in);
          continue;
        }

        System.out.println(msg);
      }
    }
     catch (IOException e) {
      System.out.println("Connection dropped.");
      serving = false;
      System.exit(-1);
    }
  }

  private void download(DataInputStream in) {
    System.out.println(fileHandler.download(in, false));
  }

  private void handleUpload(String input) {
    String[] args = input.split(" ");

    if (args.length == 1) {
      System.out.println("ERROR: invalid number of arguments for UPLOAD.");
      return;
    }

    String[] fileNames = Arrays.copyOfRange(args, 1, args.length);

    String zipResult = fileHandler.zipFiles(fileNames);

    if (!zipResult.isEmpty()) {
      System.out.println(zipResult);
      return;
    }

    sendMsg("upload");

    String response = fileHandler.upload(config.zipFileName, out);

    if (response.length() > 0) System.out.println(response);
  }

  private void handleDownload(String input) {
    if (input.split(" ").length == 1) {
      System.out.println("ERROR: invalid number of arguments for DOWNLOAD.");
      return;
    }

    sendMsg(input);
  }
}
