import java.net.Socket;
import java.util.ArrayList;

public class Client {

	private String ipAddress;
	private String computerName;
	private Socket cSocket;

	public Client(String ipAddress, String computerName, Socket cSocket) {
		this.ipAddress = ipAddress;
		this.computerName = computerName;
		this.cSocket = cSocket;
	};

	public Client(String ipAddress, String computerName) {
		this.ipAddress = ipAddress;
		this.computerName = computerName;
		this.cSocket = null;
	};

	public Client() { };

	public String getIpAddress() {
		return this.ipAddress;
	}

	public String getComputerName() {
		return this.computerName;
	}

	public Socket getSocket() {
		return this.cSocket;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public void setSocket(Socket socket) {
		this.cSocket = socket;
	}

	public Client getClientByID(String ip, ArrayList<Client> clients) {
		for(int i = 0; i < clients.size(); i++) {

			if(clients.get(i).getIpAddress().compareTo(ip) == 0)

				return clients.get(i);
		}

		return null;
	}

	public Client getClientBySocket(Socket cSocket, ArrayList<Client> clients) {
		for(int i = 0; i < clients.size(); i++) {

			if(clients.get(i).getSocket().equals(cSocket))

				return clients.get(i);

		}

		return null;
	}

}
