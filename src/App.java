import java.util.Scanner;

public class App {

	public void runApp(String[] args) {
		System.out.println("Welcome to our custom network!");

		Scanner scanner = new Scanner(System.in);
		Config config = new Config();

		new Thread(() -> { new NetworkServer().serve(); }).start();

		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {}

		String ip = getIP(scanner, config);

		auth(scanner, config);

		new NetworkClient(ip).serve();

		scanner.close();
		System.out.println("Bye!");
		System.exit(0);
	}

	private String getIP(Scanner scanner, Config config) {
		String ip = "";

		do {
			System.out.println("Please input ip address:");
			ip = scanner.nextLine();
		} while(!config.isValidIP(ip));

		return ip;
	}

	/**
   * Function to authorize the user
   */
  private void auth(Scanner scanner, Config config) {

    System.out.print("\nUsername: ");
    String username = scanner.nextLine();
    System.out.print("Password: ");
    String password = scanner.nextLine();

    while(!config.validLogin(username, password)) {
      System.out.println("\nInvalid username or password. Please try again!");
      System.out.print("\nUsername: ");
      username = scanner.nextLine();
      System.out.print("Password: ");
      password = scanner.nextLine();
    }
  }

	public static void main(String[] args) {
		new App().runApp(args);
	}
}
