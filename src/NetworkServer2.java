import java.io.*;
import java.net.*;
import java.util.*;

public class NetworkServer2 {

	private ServerSocket TCPsocket;
	private DatagramSocket UDPsocket;
	private ArrayList<Client> clients = new ArrayList<Client>();
	private Config config;
	private Scanner scanner;

	public NetworkServer2() {
		config = new Config();
		scanner = new Scanner(System.in);

		try {
			UDPsocket = new DatagramSocket(config.UDP_PORT);
		} catch (SocketException e) {
			System.out.println("Oops.. Error while creating UDP socket");
			e.printStackTrace();
		}

		try {
			TCPsocket = new ServerSocket(config.TCP_PORT);
		} catch (IOException e) {
			System.out.println("Oops.. Error while creating TCP socket");
			e.printStackTrace();
		}

	}

	public void start() throws IOException {
		new Thread(() -> {
			try {
				while(true) {
					UDPhandler();
				}
			} catch (IOException e) {
				System.out.println("Oops.. Error while handling UDP");
				e.printStackTrace();
			}
		}).start();

		TCPhandler();
	}

	public void UDPhandler() throws IOException {

		new Thread(() -> {
			broadcastMsg(config.BROADCAST_MESSAGE);
		}).start();

		receive();

	}

	public void broadcastMsg(String message) {
		try {
			byte[] msg = message.getBytes();

			InetAddress dest = InetAddress.getByName("255.255.255.255");

			DatagramPacket packet = new DatagramPacket(msg, msg.length, dest, config.UDP_PORT);

			System.out.println("Broadcasting UDP...");
			UDPsocket.send(packet);
		} catch (IOException e) {
			System.out.println("Oops.. Error while broadcasting");
			e.printStackTrace();
		}
	}

	public void sendMsgUDP(String msg, String ipAddress) throws IOException {
		InetAddress destination = InetAddress.getByName(ipAddress);

		DatagramPacket packet = new DatagramPacket(msg.getBytes(), msg.length(), destination, config.UDP_PORT);

		UDPsocket.send(packet);
	}

	public void receive() {
		try {

			DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);

			UDPsocket.receive(packet);

			byte[] data = packet.getData();

			String str = new String(data, 0, packet.getLength());

			if (str.equals(config.BROADCAST_MESSAGE)
					&& !packet.getAddress().equals(InetAddress.getLocalHost())) {

				System.out.println("Received UDP broadcass");

				String computerName = getComputerName();

				System.out.println("Trying to establish a connection with " + packet.getAddress());

				sendMsgUDP(computerName, packet.getAddress().toString());
			}

			else {
				String computerName = str;

				String ipAddress = packet.getAddress().toString().trim();

				Client client = new Client(ipAddress, computerName);

				synchronized (clients) {
					clients.add(client);
				}
			}
		} catch (IOException e) {

			System.out.println("Oops.. Error");
			e.printStackTrace();

		}
	}

	private String getComputerName() {
		System.out.println("Please input your computer name:");
		return scanner.nextLine();
	}

	public void TCPhandler() throws IOException {
		while (true) {

			Socket cSocket = TCPsocket.accept();
			Client TCPclient = getClientByID(cSocket.getInetAddress().toString().trim());
			TCPclient.setSocket(cSocket);

			new Thread(() -> {
				try {

					serve(TCPclient);

				} catch (IOException e) {

					System.err.println("Connection dropped.");

				}

				synchronized (clients) {

					System.out.println(TCPclient.getComputerName());
					String offlinemsg = TCPclient.getComputerName() + " quit the network.";
					forward(offlinemsg.getBytes(), offlinemsg.getBytes().length);
					clients.remove(TCPclient);

				}

			}).start();
		}
	}

	public Client getClientByID(String ip) {
		for(int i = 0; i < clients.size(); i++) {

			if(clients.get(i).getIpAddress().compareTo(ip) == 0)

				return clients.get(i);
		}

		return null;
	}

	public Client getClientBySocket(Socket cSocket) {
		for(int i = 0; i < clients.size(); i++) {

			if(clients.get(i).getSocket().equals(cSocket))

				return clients.get(i);

		}

		return null;
	}

	private void serve(Client client) throws IOException {
		byte[] buffer = new byte[1024];

		System.out.printf(client.getComputerName() + ": established a connection to host %s:%d\n\n ", client.getSocket().getInetAddress(), client.getSocket().getPort());

		DataInputStream in = new DataInputStream(client.getSocket().getInputStream());

		while (true) {

			int len = in.readInt();
			in.read(buffer, 0, len);
			System.out.println("Command: " + buffer.toString()); // COMMANDS TO BE EXECUTED HERE AND SENT BACK

		}

	}

	private void sendMsgTCP(String msg, Client client) {
		try {

			Socket socket = client.getSocket();
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			out.writeInt(msg.length());
			out.write(msg.getBytes(), 0, msg.length());

		} catch (IOException e) {

			System.out.println("Unable to send message to the server!");

		}
	}

	private void forward(byte[] data, int len) {
		synchronized (clients) {

			for (int i = 0; i < clients.size(); i++) {

				try {

					Socket socket = clients.get(i).getSocket();
					DataOutputStream out = new DataOutputStream(socket.getOutputStream());
					out.writeInt(len);
					out.write(data, 0, len);

				} catch (IOException e) {
					// the connection is dropped but the socket is not yet removed.
				}
			}

		}
	}

}
