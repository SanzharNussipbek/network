import java.io.*;
import java.util.*;

public class MyCMD {

	private FileHandler fileHandler;
	private Config config;

	private Map<String, String> COMMANDS;

	public MyCMD() {
		fileHandler = new FileHandler();
		config = new Config();
		initCommands();
	};

	private void initCommands() {
		COMMANDS = new HashMap<String, String>() {
			{

				put("dir",      "List files in the current directory:  dir [directory, ...]");
				put("del",      "Delete files.:                        del [filename, ...]");
				put("rd",       "Delete subdirectories.:               rd [directory, ...]");
				put("md",       "Create subdirectories.                md [directory, ...]");
				put("touch",    "Create files.                         touch [filename, ...]");
				put("cls",      "Clear console (only for MacOS).");
				put("rename",   "Rename a file.:                       rename oldName newName");
				put("help",     "List all available commands.");
				put("exit",     "Exit the system.");
				put("details",  "Get files details.:                   details [filename, ...]");
				put("upload",   "Upload files:                         upload [filename, ...]");
				put("download", "Download files:                       download [filename, ...]");
				put("cat",      "Print file content:                   cat [filename, ...]");
				put("echo",     "Write text to a file:                 echo filename textToAppend");

			}
		};
	}

	public String[] get_args(String line) {
		String[] args = line.split(" ");

		for (int i = 0; i < args.length; i++) {

			args[i] = args[i].strip();
		}

		return args;
	}

	public Boolean isValidCommand(String command) {
		return COMMANDS.containsKey(command.toLowerCase());
	}

	public String handleCommand(String[] args) {
		args[0] = args[0].toLowerCase();

		if (!isValidCommand(args[0])) {
			return "ERROR: unknown command \"" + args[0] + "\".\nPlease use HELP to get the list of available commands.";
		}

		if ((args.length == 1 && (args[0].equals("del")
															|| args[0].equals("rd")
															|| args[0].equals("touch")
															|| args[0].equals("md")
															|| args[0].equals("cat")
															|| args[0].equals("details")
															|| args[0].equals("upload")
															|| args[0].equals("download")))
					|| (args.length > 1 && (args[0].equals("exit")
																	|| args[0].equals("help")
																	|| args[0].equals("cls")))
					|| (args.length != 3 && args[0].equals("rename"))
					|| (args.length < 3 && args[0].equals("echo"))) {

					return "ERROR: invalid number of arguments for " + args[0].toUpperCase();
		}

		return executeCommand(args);
	}

	public String executeCommand(String[] args) {
		switch (args[0]) {
			case "dir":
				return my_dir(args);

			case "md":
				return my_md(args);

			case "del":
				return my_del(args);

			case "rd":
				return my_rd(args);

			case "touch":
				return my_touch(args);

			case "cat":
				return my_cat(args);

			case "cls":
				my_cls();
				return "";

			case "echo":
				return my_echo(args);

			case "rename":
				return my_rename(args);

			case "help":
				return my_help();

			case "details":
				return my_details(args);

			case "exit":
				System.exit(0);
				return "";

			default:
				return "ERROR: unknown command.";
		}
	}

	public String my_del(String[] args) {
		for (int i = 1; i < args.length; i++) {

			if (args[i].contains("\\")) {
				args[i] = fileHandler.prepareFilename(args[i]);

				String[] paths = args[i].split("\\\\");

				if (paths.length == 1) {
					File file = new File(paths[0]);
					if (fileHandler.checkFileForDel(file) != 0)
						return config.MESSAGES[fileHandler.checkFileForDel(file)];

					file.delete();

				} else {
					String path = fileHandler.getFullPath(paths);
					File file = new File(path);
					if (fileHandler.checkFileForDel(file) != 0)
						return config.MESSAGES[fileHandler.checkFileForDel(file)];

					file.delete();
				}
			} else {
				File file = new File(args[i]);
				if (fileHandler.checkFileForDel(file) != 0)
					return config.MESSAGES[fileHandler.checkFileForDel(file)];

				file.delete();
			}
		}

		return "File" + (args.length > 2 ? "s" : "") + " deleted.";
	}

	public String my_rd(String[] args) {
		String dirname;
		for (int i = 1; i < args.length; i++) {

			if (args[i].contains("\\")) {

				args[i] = fileHandler.prepareFilename(args[i]);

				String[] paths = args[i].split("\\\\");

				dirname = paths.length == 1 ? paths[0] : fileHandler.getFullPath(paths);

			} else {
				dirname = args[i];
			}

			File file = new File(dirname);
			if (fileHandler.checkDirForRD(file) != 0)
				return config.MESSAGES[fileHandler.checkDirForRD(file)];

			file.delete();
		}

		return "Director" + (args.length > 2 ? "ies" : "y") + " deleted.";
	}

	public String my_md(String[] args) {
		for (int i = 1; i < args.length; i++) {

			args[i] = fileHandler.prepareFilename(args[i]);

			String[] paths = args[i].split("\\\\");

			if (paths.length == 1) {

				new File(paths[0]).mkdir();

			} else {

				String path = fileHandler.getFullPath(paths);
				new File(path).mkdirs();

			}
		}

		return "Director" + (args.length > 2 ? "ies" : "y") + " created.";
	}

	public String my_dir(String[] args) {
		File path;
		String files;
		String res = "";

		if (args.length == 1) {

			path = new File(".");
			files = String.join("\n  ", path.list());
			return path + ":\n  " + files;

		}

		for (int i = 1; i < args.length; i++) {

			args[i] = fileHandler.prepareFilename(args[i]);
			path = new File(args[i]);

			if (!path.exists()) {
				res += "Directory \"" + path + "\" does not exist in the current directory." + (i < args.length - 1 ? "\n\n" : "");
				continue;
			}

			if (!path.isDirectory()) {
				res += "File \"" + path + "\" is not a directory." + (i < args.length - 1 ? "\n\n" : "");
				continue;
			}

			files = String.join("\n  ", path.list());
			res += String.format("%s:\n  %s", path, files) + (i != args.length - 1 ? "\n\n" : "");
		}

		return res;
	}

	public String my_touch(String[] args) {
		String result = "";
		String filename;

		for (int i = 1; i < args.length; i++) {

			args[i] = fileHandler.prepareFilename(args[i]);

			String[] paths = args[i].split("\\\\");

			filename = paths.length == 1 ? paths[0] : fileHandler.getFullPath(paths);

			if (fileHandler.createFile(filename) != 0)
				result += config.MESSAGES[fileHandler.createFile(filename)] + (i < args.length - 1 ? "\n\n" : "");
		}

		return result.length() != 0 ? result : "File" + (args.length > 2 ? "s" : "") + " created.";
	}

	public void my_cls() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

	public String my_rename(String[] args) {
		if (args.length != 3) {
			return String.format("ERROR: invalid number of arguments for rename.");
		}

		String oldName = args[1];
		String newName = args[2];

		File oldFile = new File(oldName);
		File newFile = new File(newName);

		if (fileHandler.validateFile(oldFile) != 0)
			return config.MESSAGES[fileHandler.validateFile(oldFile)];

		boolean result = oldFile.renameTo(newFile);

		if (!result) {
			return String.format("Oops.. Error while renaming \"" + oldName + "\" to \"" + newName + "\".");
		}

		return "File renamed successfullyю";
	}

	public String my_help() {
		String res = "Available commands:\n";

		for (String command : COMMANDS.keySet()) {
			res += String.format("\t%-10s - %s\n", command, COMMANDS.get(command));
		}

		return res;
	}

	public String my_details(String[] args) {
		File[] files = new File[args.length - 1];

		for (int i = 1; i < args.length; i++) {

			files[i - 1] = new File(args[i].trim());

		}

		String res = "";
		String details = "";

		for (int i = 0; i < files.length; i++) {

			File file = files[i];

			if (fileHandler.validateFile(file) != 0) {
				res += config.MESSAGES[fileHandler.validateFile(file)] + '\n';
				continue;
			}

			details += file.getName() + ":\n";
			details += String.format("\t%-24s: %s\n", "name",                file.getName());
			details += String.format("\t%-24s: %s\n", "size (bytes)",        file.length());
			details += String.format("\t%-24s: %s\n", "absolute path?",      file.isAbsolute());
			details += String.format("\t%-24s: %s\n", "exists?",             file.exists());
			details += String.format("\t%-24s: %s\n", "hidden?",             file.isHidden());
			details += String.format("\t%-24s: %s\n", "dir?",                file.isDirectory());
			details += String.format("\t%-24s: %s\n", "file?",                file.isFile());
			details += String.format("\t%-24s: %s\n", "modified (timestamp)", file.lastModified());
			details += String.format("\t%-24s: %s\n", "readable?",           file.canRead());
			details += String.format("\t%-24s: %s\n", "writable?",           file.canWrite());
			details += String.format("\t%-24s: %s\n", "executable?",         file.canExecute());
			details += String.format("\t%-24s: %s\n", "parent",              file.getParent());
			details += String.format("\t%-24s: %s\n", "absolute file",        file.getAbsoluteFile());

			try {
				details += String.format("\t%-24s: %s\n", "canonical file",     file.getCanonicalFile());
			} catch (IOException e) {
			}

			try {
				details += String.format("\t%-24s: %s\n", "canonical path",    file.getCanonicalPath());
			} catch (IOException e) {
			}

			details += String.format("\t%-24s: %s\n", "partition space (bytes)", file.getTotalSpace());
			details += String.format("\t%-24s: %s", "usable space (bytes)",      file.getUsableSpace());

			res += details + (i != files.length - 1 ? "\n\n" : "");
			details = "";
		}

		return res;
	}

	private String my_cat(String[] args) {
		String res = "";
		String fileInfo = "";
		File file;

		for (int i = 1; i < args.length; i++) {
			file = new File(args[i]);
			if (fileHandler.validateFile(file) != 0) {
				res += config.MESSAGES[fileHandler.validateFile(file)];
				continue;
			}
			fileInfo += args[i] + ":\n";
			fileInfo += fileHandler.readFromFile(file) + (i < args.length - 1 ? "\n\n" : "");

			res += fileInfo;
			fileInfo = "";
		}
		return res;
	}

	private String my_echo(String[] args) {
		File file = new File(args[1]);

		if (fileHandler.validateFile(file) != 0) {
			return config.MESSAGES[fileHandler.validateFile(file)];
		}

		String text = String.join(" ", Arrays.copyOfRange(args, 2, args.length));

		fileHandler.writeToFile(file, text);

		return "";
	}

}
